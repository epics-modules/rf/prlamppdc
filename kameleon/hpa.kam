# author: Hinko Kocevar
# email: hinko.kocevar@esss.se
# copyright: (C) 2015-2016 European Spallation Source (ESS)
# version: 1.0.0
# date: 2016/MAY/17
# description: File that describes the commands/statuses that the tool Kameleon
#              receives/sends from/to clients through the TCP/IP protocol when
#              working with AK-NORD XT-Pico-SXL board.



# The terminator (EOL) of commands/statuses is described in the "TERMINATOR" variable. By default, the terminator is empty. If defined, the terminator is inserted at the end of the commands/statuses received/sent from/to clients before Kameleon starts to process these. The generic form of this variable is:
#
#    TERMINATOR = value
#
# Where value can either be an arbitrary string (e.g. "END") or one of the following pre-defined terminators:
#
#    LF     : the terminator is a line feed (0xA).
#    CR     : the terminator is a carriage return (0xD).
#    LF + CR: the terminator is a line feed (0xA) followed by a carriage return (0xD).
#    CR + LF: the terminator is a carriage return (0xD) followed by a line feed (0xA).

#TERMINATOR = CR + LF



# Data (i.e. commands) received from the client are described in the "COMMANDS" list. The generic form of this list is:
#
#    COMMANDS = [[description_1, command_1, status_1, wait_1], [description_2, command_2, status_2, wait_2], ..., [description_X, command_X, status_X, wait_X]]
#
# Where:
#
#    description: (mandatory) string that describes the command (e.g. "Turn power on").
#    command    : (mandatory) string that represents the command (e.g. "AC1"). Only data (received from the client) that matches exactly the command is selected. Additional matching policies are available:
#                    - if command starts with "***", then any data (received from the client) that ends with command is selected.
#                    - if command ends with "***", then any data (received from the client) that starts with command is selected.
#                    - if command starts and ends with "***", then any data (received from the client) that contains the command is selected.
#    status     : (optional) integer or list that specifies the index(es) of the status(es) (stored in the "STATUSES" list) to send to the client after the command is selected. The first status (stored in "STATUSES" list) is at index 1. If set to 0 or not specified, then no status is sent.
#    wait       : (optional) integer that specifies the time to wait (in milliseconds) before sending the status to the client. If set to 0 or not specified, then the status is immediately sent (i.e. right after the command is received).

# XT-Pico-SXL takes binary commands over TCP/IP socket.
COMMANDS = [
            ["Test",        "0d0d0d0d0d".decode("HEX"), 1],
            ["InPwr",       "11".decode("HEX"), 2],
            ["RefPwr",      "33".decode("HEX"), 3],
            ["OutPwr",      "55".decode("HEX"), 4],
            ["Temp",        "77".decode("HEX"), 5],
            ["Status",      "AA".decode("HEX"), 6],
            ["readPwr",      "00000000".decode("HEX"), 7], # for InPwr, RefPwr, OutPwr, Temp
            ["readStatus",   "00".decode("HEX"), 8], 
#            ["turnOn",   "0x7E".decode("HEX"), 9], 
#            ["turnOff",   "0x95".decode("HEX"), 9], 
           ]



# Data (i.e. statuses) sent to the client are described in the "STATUSES" list. The generic form of this list is:
#
#    STATUSES = [[description_1, behavior_1, value_1, prefix_1, suffix_1, timeout_1], [description_2, behavior_2, value_2, prefix_2, suffix_2, timeout_2]], ..., [description_X, behavior_X, value_X, prefix_X, suffix_X, timeout_X]]
#
# Where:
#
#    description: (mandatory) string that describes the status (e.g. "Get power status").
#    behavior   : (mandatory) integer that specifies the behavior for generating the status. It can either be:
#                    - FIXED (sends a fixed value to the client)
#                    - ENUM (sends a value - belonging to an enumeration - to the client)
#                    - INCR (sends an incremented value to the client)
#                    - RANDOM (sends a random value to the client)
#                    - CUSTOM (sends a custom value to the client)
#    value      : (mandatory) value to send to the client. Depending on the behavior, it can either be an integer, float, string or list:
#                    - when FIXED, the value is expected to be an integer, float or string. Independently of how many times it is sent to the client, the value remains the same (i.e. does not change).
#                    - when ENUM, the value is expected to be a list. It represents a set of elements (enumeration). After sending an element of the list to the client, the next value to be sent is the next element in the list. When the last element is sent, the next to be sent is the the first element of the list.
#                    - when INCR, the value is expected to be an integ55555555er, float or list. If an integer, then the first value to be sent is a 0 and subsequent values to be sent are incremented by value. If a list, then the lower bound, upper bound and increment values are defined by the first, second and third elements of the list, respectively.
#                    - when RANDOM, the value is expected to be an integer or a list. If an integer, then a random number between 0 and value is generated. If a list, then the lower and upper bounds of the random number to generate are defined by the first and second elements of the list, respectively. The generated random number is sent to the client.
#                    - when CUSTOM, the value is expected to be a string. It contains the name of an user-defined Python function to be called by Kameleon. The returned value of this function is sent to the client.
#    prefix     : (optional) string that contains the prefix to insert at the beginning of the value to send to the client. If not specified, then nothing is inserted.
#    suffix     : (optional) string that contains the suffix to insert at the end of the value to send to the client. If not specified, then nothing is inserted.
#    timeout    : (optional) integer that specifies the time-out (in milliseconds) after which the status is sent to the client (i.e. time-based). If set to 0 or not specified, then the status is only sent after receiving a command from the client (i.e. event-based).

# XT-Pico-SXL sends binary responses over TCP/IP socket.
STATUSES = [
            ["Test",     FIXED, "0d01020304".decode("HEX")], 
            ["InPwr",    CUSTOM, "func_set_pwr(1)"],  
            ["RefPwr",    CUSTOM, "func_set_pwr(2)"],  
            ["OutPwr",    CUSTOM, "func_set_pwr(3)"],  
            ["Temp",    CUSTOM,   "func_set_pwr(4)"], 
            ["Status",  FIXED,   "func_set_stat()"], # Temperature high and amp on
            ["InPwrRes",  CUSTOM,   "func_get_pwr()"], 
            ["StatusRes",  CUSTOM,   "func_get_stat()"], 
            ["OnOffOK",  FIXED,  "00".decode("HEX")], 
           ]

# pwr = 1 : in pwr
# pwr = 2 : ref pwr 
# pwr = 3 : out pwr
# pwr = 4 : temperature
pwr = 0

# amp = 0 : 352
# amp = 1 : 704
in_amp = 1
ref_amp = 1
out_amp = 1
tmp_amp = 1

in_values = ["01010101".decode("HEX"), "02010101".decode("HEX")] # 0.207059 , 0.413309
ref_values = ["02020202".decode("HEX"), "03020202".decode("HEX")] # 0.414118  , 0.620368
out_values = ["03030303".decode("HEX"), "04020202".decode("HEX")] # 0.621176  , 0.826618
tmp_values = ["04040404".decode("HEX"), "05040404".decode("HEX")] # 0.828235, 1.03449

stat_amp = 1

stat_values = ["03".decode("HEX"), "01".decode("HEX")]

def func_set_pwr(set_pwr):
    global pwr
    global in_amp
    global ref_amp
    global out_amp
    global tmp_amp

    pwr = set_pwr
    if set_pwr == 1:
        in_amp = (in_amp + 1) % 2
    elif set_pwr == 2:
        ref_amp = (ref_amp + 1) % 2
    elif set_pwr == 3:
        out_amp = (out_amp + 1) % 2
    elif set_pwr == 4:
        tmp_amp = (tmp_amp + 1) % 2

    return "00".decode("HEX")

def func_get_pwr():
    global pwr
    global amp
    global in_values
    global ref_values
    global out_values
    global tmp_values

    if pwr == 1: # In pwr
        return in_values[in_amp] 
    if pwr == 2: # ref pwr
        return ref_values[ref_amp] 
    if pwr == 3: # out pwr
        return out_values[out_amp] 
    if pwr == 4: # temperature
        return tmp_values[tmp_amp] 

def func_set_stat():
    global stat_amp

    stat_amp = (stat_amp + 1) % 2

    return "00".decode("HEX")

def func_get_stat():
    global stat_amp
    global stat_values

    return stat_values[stat_amp] 
