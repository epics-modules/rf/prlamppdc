record(ao, "$(P)$(R)Temp-S")
{
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_TMPSP")
    field(PREC, "4")
    field(EGU, "degC")
}

record(ai, "$(P)$(R)Temp-RB")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_TMPSP")
    field(PREC, "4")
    field(EGU, "degC")
}

record(longin,      "$(P)$(R)Humidity-R")
{   field(DESC, "PDC Humidity")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_HUM")
    field(EGU,  "%")
}

record(ai,      "$(P)$(R)PhaseErr352-R")
{   field(DESC, "Phase error 352")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PHERR352")
    field(PREC, "2")
    field(EGU,  "mdeg")
}

record(ai,      "$(P)$(R)PhaseErr704-R")
{   field(DESC, "Phase error 704")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PHERR704")
    field(PREC, "2")
    field(EGU,  "mdeg")
}

record(ai,      "$(P)$(R)AttenuatorVolt352-R")
{   field(DESC, "Attenuator 352 control voltage")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_ATTVOLT352")
    field(EGU,  "mV")
}

record(ai,      "$(P)$(R)AttenuatorVolt704-R")
{   field(DESC, "Attenuator 704 control voltage")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_ATTVOLT704")
    field(EGU,  "mV")
}

record(ai,      "$(P)$(R)PhShifterVolt352-R")
{   field(DESC, "PhaseShifter 352 control voltage")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PHSHIFTV352")
    field(EGU,  "mV")
}

record(ai,      "$(P)$(R)PhShifterVolt704-R")
{   field(DESC, "PhaseShifter 704 control voltage")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PHSHIFTV704")
    field(EGU,  "mV")
}

record(ai, "$(P)$(R)PhaseDetec0-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PD0")
    field(PREC, "3")
    field(SCAN, "Passive")
    field(EGU, "Degree")
}

record(ai, "$(P)$(R)PhaseDetec1-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PD1")
    field(PREC, "3")
    field(SCAN, "Passive")
    field(EGU, "Degree")
}

record(ai, "$(P)$(R)PhaseDetec2-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PD2")
    field(PREC, "3")
    field(SCAN, "Passive")
    field(EGU, "Degree")
}

record(ai, "$(P)$(R)PhaseDetec3-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PD3")
    field(PREC, "3")
    field(SCAN, "Passive")
    field(EGU, "Degree")
}

record(ai, "$(P)$(R)PhaseDetec4-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PD4")
    field(PREC, "3")
    field(SCAN, "Passive")
    field(EGU, "Degree")
}

record(ai, "$(P)$(R)PhaseDetecPower0-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PD0PWR")
    field(PREC, "3")
    field(SCAN, "Passive")
    field(EGU, "dB")
}

record(ai, "$(P)$(R)PhaseDetecPower1-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PD1PWR")
    field(PREC, "3")
    field(SCAN, "Passive")
    field(EGU, "dB")
}
record(ai, "$(P)$(R)PhaseDetecPower2-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PD2PWR")
    field(PREC, "3")
    field(SCAN, "Passive")
    field(EGU, "dB")
}
record(ai, "$(P)$(R)PhaseDetecPower3-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PD3PWR")
    field(PREC, "3")
    field(SCAN, "Passive")
    field(EGU, "dB")
}
record(ai, "$(P)$(R)PhaseDetecPower4-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PD4PWR")
    field(PREC, "3")
    field(SCAN, "Passive")
    field(EGU, "dB")
}

record(ai, "$(P)$(R)PowerDetec0-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PWD0")
    field(PREC, "3")
    field(SCAN, "Passive")
    field(EGU, "dBm")
}

record(ai, "$(P)$(R)PowerDetec1-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PWD1")
    field(PREC, "3")
    field(SCAN, "Passive")
    field(EGU, "dBm")
}

record(ai, "$(P)$(R)PowerDetec2-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PWD2")
    field(PREC, "3")
    field(SCAN, "Passive")
    field(EGU, "dBm")
}

record(ai, "$(P)$(R)PowerDetec3-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PWD3")
    field(PREC, "3")
    field(SCAN, "Passive")
    field(EGU, "dBm")
}

record(ai, "$(P)$(R)Temp-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_TMP")
    field(PREC, "4")
    field(SCAN, "Passive")
    field(EGU, "degC")
}


record(ao, "$(P)$(R)PowerPID352-S")
{
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PWRPID352")
    field(PREC, "3")
    field(EGU, "dBm")
    field(DRVH, "0")
    field(DRVL, "-65.536")
}

record(ai, "$(P)$(R)PowerPID352-RB")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PWRPID352")
    field(PREC, "3")
    field(SCAN, "Passive")
    field(EGU, "dBm")
}

record(ao, "$(P)$(R)PowerPID704-S")
{
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PWRPID704")
    field(PREC, "3")
    field(EGU, "dBm")
    field(DRVH, "0")
    field(DRVL, "-65.536")
}

record(ai, "$(P)$(R)PowerPID704-RB")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_PWRPID704")
    field(PREC, "3")
    field(SCAN, "Passive")
    field(EGU, "dBm")
}

record(mbbi, "$(P)$(R)RecalibStatus-RB")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_RECAL")
    field(SCAN, "Passive")
    field(ZRVL, "0")
    field(ZRST, "Calibrated")
    field(ONVL, "1")
    field(ONST, "Phase PID352 HIGH")
    field(TWVL, "2")
    field(TWST, "Phase PID352 LOW")
    field(THVL, "3")
    field(THST, "Phase PID704 HIGH")
    field(FRVL, "4")
    field(FRST, "Phase PID704 LOW")
    field(FVVL, "5")
    field(FVST, "Power PID352 HIGH")
    field(SXVL, "6")
    field(SXST, "Power PID352 LOW")
    field(SVVL, "7")
    field(SVST, "Power PID704 HIGH")
    field(EIVL, "8")
    field(EIST, "Power PID704 LOW")
    field(NIVL, "9")
    field(NIST, "PDC input power too LOW")
    field(ONSV, "MAJOR")
    field(TWSV, "MAJOR")
    field(THSV, "MAJOR")
    field(FRSV, "MAJOR")
    field(FVSV, "MAJOR")
    field(SXSV, "MAJOR")
    field(SVSV, "MAJOR")
    field(EISV, "MAJOR")
    field(NISV, "MAJOR")
}


record(mbbi, "$(P)$(R)CompStatus-RB")
{
    field(DESC, "Compensation Status")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_COMP")
    field(SCAN, "Passive")
    field(ZRVL, "0")
    field(ZRST, "Calibration on going")
    field(ONVL, "1")
    field(ONST, "Mods OK.Amps/ cabs comp")
    field(TWVL, "2")
    field(TWST, "Mods OK.Amps comp")
    field(THVL, "3")
    field(THST, "Invalid status")
}


record(bo, "$(P)$(R)Recalib")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_RECAL")
    field(ONAM, "On")
    field(ZNAM, "Off")
#    field(SCAN, "Passive")
    field(SDIS, "$(P)$(R)RecalibStatus-RB")
}


record(bo, "$(P)$(R)CompAmpCabl")
{
    field(DESC, "Compensate Amplifiers and Cables")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_COMP_CAB")
    field(ONAM, "On")
    field(ZNAM, "Off")
    field(SCAN, "Passive")
}


record(bo, "$(P)$(R)CompAmp")
{
    field(DESC, "Compensate Amplifiers")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_COMP_AMP")
    field(ONAM, "On")
    field(ZNAM, "Off")
    field(SCAN, "Passive")
}

### Amplifier PVs

record(ai, "$(P)$(R_AMP)InPwr352Raw-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_352_INPWR")
    field(PREC, "5")
    field(EGU, "V")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R_AMP)InPwr704Raw-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_704_INPWR")
    field(PREC, "5")
    field(EGU, "V")
    field(SCAN, "Passive")
}


record(ai, "$(P)$(R_AMP)RefPwr352Raw-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_352_REFPWR")
    field(PREC, "5")
    field(EGU, "V")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R_AMP)RefPwr704Raw-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_704_REFPWR")
    field(PREC, "5")
    field(EGU, "V")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R_AMP)OutPwr352Raw-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_352_OUTPWR")
    field(PREC, "5")
    field(EGU, "V")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R_AMP)OutPwr704Raw-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_704_OUTPWR")
    field(PREC, "5")
    field(EGU, "V")
    field(SCAN, "Passive")
}


record(ai, "$(P)$(R_AMP)Temp352Raw-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_352_TEMP")
    field(PREC, "5")
    field(EGU, "V")
    field(SCAN, "Passive")
}


record(ai, "$(P)$(R_AMP)Temp704Raw-R")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_704_TEMP")
    field(PREC, "5")
    field(EGU, "V")
    field(SCAN, "Passive")
}

record(bi, "$(P)$(R_AMP)TempStat352-R")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_352_TEMPSTAT")
    field(ZNAM, "OK")
    field(ONAM, "HIGH")
    field(OSV, "MAJOR")
    field(SCAN, "Passive")
}

record(bi, "$(P)$(R_AMP)TempStat704-R")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_704_TEMPSTAT")
    field(ONAM, "HIGH")
    field(ZNAM, "OK")
    field(OSV, "MAJOR")
    field(SCAN, "Passive")
}

record(bi, "$(P)$(R_AMP)OnOff352-R")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_352_AMPSTAT")
    field(ONAM, "On")
    field(ZNAM, "Off")
    field(SCAN, "Passive")
}

record(bi, "$(P)$(R_AMP)OnOff704-R")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_704_AMPSTAT")
    field(ONAM, "On")
    field(ZNAM, "Off")
    field(SCAN, "Passive")
}

record(bo, "$(P)$(R_AMP)OnOff352-S")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_352_ONOFF")
    field(ONAM, "On")
    field(ZNAM, "Off")
    field(VAL, "0")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

record(bo, "$(P)$(R_AMP)OnOff704-S")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PRLAMPPDC_704_ONOFF")
    field(ONAM, "On")
    field(ZNAM, "Off")
    field(VAL, "0")
    field(PINI, "YES")
    field(SCAN, "Passive")
}



### Records to coordinate the reading

record(seq, "$(P)$(R)#ProcRead1") {
    field(DESC, "Inter. Process all read PVs")
    field(DLY0, "$(DLY=0.05)")
    field(DLY1, "$(DLY=0.05)")
    field(DLY2, "$(DLY=0.05)")
    field(DLY3, "$(DLY=0.05)")
    field(DLY4, "$(DLY=0.05)")
    field(DLY5, "$(DLY=0.05)")
    field(DLY6, "$(DLY=0.05)")
    field(DLY7, "$(DLY=0.05)")
    field(DLY8, "$(DLY=0.05)")
    field(DLY9, "$(DLY=0.05)")
    field(DLYA, "$(DLY=0.05)")
    field(DLYB, "$(DLY=0.05)")
    field(DLYC, "$(DLY=0.05)")
    field(DLYD, "$(DLY=0.05)")
    field(DLYE, "$(DLY=0.05)")
    field(DOL0, "1")
    field(DOL1, "1")
    field(DOL2, "1")
    field(DOL3, "1")
    field(DOL4, "1")
    field(DOL5, "1")
    field(DOL6, "1")
    field(DOL7, "1")
    field(DOL8, "1")
    field(DOL9, "1")
    field(DOLA, "1")
    field(DOLB, "1")
    field(DOLC, "1")
    field(DOLD, "1")
    field(DOLE, "1")
    field(DOLF, "1")
    field(LNK0, "$(P)$(R)PhaseDetec0-R.PROC")
    field(LNK1, "$(P)$(R)PhaseDetec1-R.PROC")
    field(LNK2, "$(P)$(R)PhaseDetec2-R.PROC")
    field(LNK3, "$(P)$(R)PhaseDetec3-R.PROC")
    field(LNK4, "$(P)$(R)PhaseDetec4-R.PROC")
    field(LNK5, "$(P)$(R)PhaseDetecPower0-R.PROC")
    field(LNK6, "$(P)$(R)PhaseDetecPower1-R.PROC")
    field(LNK7, "$(P)$(R)PhaseDetecPower2-R.PROC")
    field(LNK8, "$(P)$(R)PhaseDetecPower3-R.PROC")
    field(LNK9, "$(P)$(R)PhaseDetecPower4-R.PROC")
    field(LNKA, "$(P)$(R)PowerDetec0-R.PROC")
    field(LNKB, "$(P)$(R)PowerDetec1-R.PROC")
    field(LNKC, "$(P)$(R)PowerDetec2-R.PROC")
    field(LNKD, "$(P)$(R)PowerDetec3-R.PROC")
    field(LNKE, "$(P)$(R)Temp-R.PROC")
    field(LNKF, "$(P)$(R)#ProcRead2.PROC")
    field(SCAN, "2 second")
}

record(seq, "$(P)$(R)#ProcRead2") {
    field(DESC, "Inter. Process all read PVs")
    field(DLY0, "$(DLY=0.05)")
    field(DLY1, "$(DLY=0.05)")
    field(DLY2, "$(DLY=0.05)")
    field(DLY3, "$(DLY=0.05)")
    field(DLY4, "$(DLY=0.05)")
    field(DLY5, "$(DLY=0.05)")
    field(DLY6, "$(DLY=0.05)")
    field(DLY7, "$(DLY=0.05)")
    field(DLY8, "$(DLY=0.05)")
    field(DLY9, "$(DLY=0.05)")
    field(DLYA, "$(DLY=0.05)")
    field(DLYB, "$(DLY=0.05)")
    field(DLYC, "$(DLY=0.05)")
    field(DOL0, "1")
    field(DOL1, "1")
    field(DOL2, "1")
    field(DOL3, "1")
    field(DOL4, "1")
    field(DOL5, "1")
    field(DOL6, "1")
    field(DOL7, "1")
    field(DOL8, "1")
    field(DOL9, "1")
    field(DOLA, "1")
    field(DOLB, "1")
    field(DOLC, "1")
    field(LNK0, "$(P)$(R)Temp-R")
    field(LNK1, "$(P)$(R)Temp-RB.PROC")
    field(LNK2, "$(P)$(R)PowerPID352-RB.PROC")
    field(LNK3, "$(P)$(R)PowerPID704-RB.PROC")
    field(LNK4, "$(P)$(R)RecalibStatus-RB.PROC")
    field(LNK5, "$(P)$(R)CompStatus-RB.PROC")
    field(LNK6, "$(P)$(R)Humidity-R.PROC")
    field(LNK7, "$(P)$(R)PhaseErr352-R.PROC")
    field(LNK8, "$(P)$(R)PhaseErr704-R.PROC")
    field(LNK9, "$(P)$(R)AttenuatorVolt352-R.PROC")
    field(LNKA, "$(P)$(R)AttenuatorVolt704-R.PROC")
    field(LNKB, "$(P)$(R)PhShifterVolt352-R.PROC")
    field(LNKC, "$(P)$(R)PhShifterVolt704-R.PROC")
    field(FLNK, "$(P)$(R)#ProcRead3")
}


record(seq, "$(P)$(R)#ProcRead3") {
    field(DESC, "Inter. Process all read PVs from amp")
    field(DLY0, "$(DLY=0.01)")
    field(DLY1, "$(DLY=0.02)")
    field(DLY2, "$(DLY=0.05)")
    field(DLY3, "$(DLY=0.05)")
    field(DLY4, "$(DLY=0.05)")
    field(DLY5, "$(DLY=0.05)")
    field(DLY6, "$(DLY=0.05)")
    field(DLY7, "$(DLY=0.01)")
    field(DLY8, "$(DLY=0.02)")
    field(DLY9, "$(DLY=0.05)")
    field(DLYA, "$(DLY=0.05)")
    field(DLYB, "$(DLY=0.05)")
    field(DLYC, "$(DLY=0.05)")
    field(DLYD, "$(DLY=0.05)")
    field(DOL0, "1")
    field(DOL1, "1")
    field(DOL2, "1")
    field(DOL3, "1")
    field(DOL4, "1")
    field(DOL5, "1")
    field(DOL6, "1")
    field(DOL7, "1")
    field(DOL8, "1")
    field(DOL9, "1")
    field(DOLA, "1")
    field(DOLB, "1")
    field(DOLC, "1")
    field(DOLD, "1")
    field(LNK0, "$(P)$(R_AMP)OnOff352-R.PROC")
    field(LNK1, "$(P)$(R_AMP)OnOff352-R.PROC")
    field(LNK2, "$(P)$(R_AMP)RefPwr352Raw-R.PROC")
    field(LNK3, "$(P)$(R_AMP)OutPwr352Raw-R.PROC")
    field(LNK4, "$(P)$(R_AMP)TempStat352-R.PROC")
    field(LNK5, "$(P)$(R_AMP)InPwr352Raw-R.PROC")
    field(LNK6, "$(P)$(R_AMP)Temp352Raw-R.PROC")
    field(LNK7, "$(P)$(R_AMP)OnOff704-R.PROC")
    field(LNK8, "$(P)$(R_AMP)OnOff704-R.PROC")
    field(LNK9, "$(P)$(R_AMP)RefPwr704Raw-R.PROC")
    field(LNKA, "$(P)$(R_AMP)OutPwr704Raw-R.PROC")
    field(LNKB, "$(P)$(R_AMP)Temp704Raw-R.PROC")
    field(LNKC, "$(P)$(R_AMP)TempStat704-R.PROC")
    field(LNKD, "$(P)$(R_AMP)InPwr704Raw-R.PROC")
}
