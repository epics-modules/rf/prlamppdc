/*
 * PRL_Amp_PDC.h
 *
 *  Created on: Mar 3, 2021
 *      Author: gabrielfedel
 */

#ifndef _PRL_Amp_PDC_H_
#define _PRL_Amp_PDC_H_

#include <asynPortDriver.h>
#include <asynOctetSyncIO.h>
#include "AKBase.h"
#include "AKI2C.h"

#define HEXDUMP_COLS 8

// I2C registers
#define PRLAMPPDC_PD0_REG			  0x01
#define PRLAMPPDC_PD1_REG			0x11
#define PRLAMPPDC_PD2_REG			0x21
#define PRLAMPPDC_PD3_REG			0x31
#define PRLAMPPDC_PD4_REG			0x41

#define PRLAMPPDC_PD0PWR_REG			0x06
#define PRLAMPPDC_PD1PWR_REG			0x16
#define PRLAMPPDC_PD2PWR_REG			0x26
#define PRLAMPPDC_PD3PWR_REG			0x36
#define PRLAMPPDC_PD4PWR_REG			0x46

#define PRLAMPPDC_PWD0_REG			0x51
#define PRLAMPPDC_PWD1_REG			0x61
#define PRLAMPPDC_PWD2_REG			0x71
#define PRLAMPPDC_PWD3_REG			0x81

#define PRLAMPPDC_TMP_READ_REG		0x91
#define PRLAMPPDC_TMP_SP_REG		    0x95
#define PRLAMPPDC_HUM_REG		    0x9A

#define PRLAMPPDC_PWRPID352_REG	    0xB1
#define PRLAMPPDC_PWRPID704_REG	    0xB3

#define PRLAMPPDC_RECAL_REG	        0xA1
#define PRLAMPPDC_COMP_REG	        0xA2

#define PRLAMPPDC_COMP_CAB_VAL        0x01
#define PRLAMPPDC_COMP_AMP_VAL        0x04

#define PRLAMPPDC_CS_REG			  0xA4 // Chip selector

#define PRLAMPPDC_PHERR352_REG		    0xA6
#define PRLAMPPDC_PHERR704_REG		    0xA8

#define PRLAMPPDC_ATTVOLT352_REG		    0xBA
#define PRLAMPPDC_ATTVOLT704_REG		    0xBC
#define PRLAMPPDC_PHSHIFTV352_REG		    0xB6
#define PRLAMPPDC_PHSHIFTV704_REG		    0xB8

// SPI Registers
#define PRLAMPPDC_TestCode                   0xDD
#define PRLAMPPDC_OnCode                     0x7E
#define PRLAMPPDC_OffCode                    0x95
#define PRLAMPPDC_InPowerCode                0x11
#define PRLAMPPDC_RefPowerCode               0x33
#define PRLAMPPDC_OutPowerCode               0x55
#define PRLAMPPDC_TempCode                   0x77
#define PRLAMPPDC_Status                     0xAA

#define StatusMessageString				"PRLAMPPDC_STATUS_MSG"

#define PRLAMPPDC_PD0String				"PRLAMPPDC_PD0"
#define PRLAMPPDC_PD1String				"PRLAMPPDC_PD1"
#define PRLAMPPDC_PD2String				"PRLAMPPDC_PD2"
#define PRLAMPPDC_PD3String				"PRLAMPPDC_PD3"
#define PRLAMPPDC_PD4String				"PRLAMPPDC_PD4"

#define PRLAMPPDC_PD0PWRString				"PRLAMPPDC_PD0PWR"
#define PRLAMPPDC_PD1PWRString				"PRLAMPPDC_PD1PWR"
#define PRLAMPPDC_PD2PWRString				"PRLAMPPDC_PD2PWR"
#define PRLAMPPDC_PD3PWRString				"PRLAMPPDC_PD3PWR"
#define PRLAMPPDC_PD4PWRString				"PRLAMPPDC_PD4PWR"

#define PRLAMPPDC_PWD0String				"PRLAMPPDC_PWD0"
#define PRLAMPPDC_PWD1String				"PRLAMPPDC_PWD1"
#define PRLAMPPDC_PWD2String				"PRLAMPPDC_PWD2"
#define PRLAMPPDC_PWD3String				"PRLAMPPDC_PWD3"

#define PRLAMPPDC_TMPString				"PRLAMPPDC_TMP"
#define PRLAMPPDC_TMPSPString			"PRLAMPPDC_TMPSP"
#define PRLAMPPDC_HUMString			  "PRLAMPPDC_HUM"

#define PRLAMPPDC_PWRPID352String				"PRLAMPPDC_PWRPID352"
#define PRLAMPPDC_PWRPID704String				"PRLAMPPDC_PWRPID704"

#define PRLAMPPDC_RECALString				"PRLAMPPDC_RECAL"
#define PRLAMPPDC_COMPString				"PRLAMPPDC_COMP"
#define PRLAMPPDC_COMP_CABString				"PRLAMPPDC_COMP_CAB"
#define PRLAMPPDC_COMP_AMPString				"PRLAMPPDC_COMP_AMP"

#define PRLAMPPDC_PHERR352String				"PRLAMPPDC_PHERR352"
#define PRLAMPPDC_PHERR704String				"PRLAMPPDC_PHERR704"

#define PRLAMPPDC_ATTVOLT352String				"PRLAMPPDC_ATTVOLT352"
#define PRLAMPPDC_ATTVOLT704String				"PRLAMPPDC_ATTVOLT704"
#define PRLAMPPDC_PHSHIFTV352String				"PRLAMPPDC_PHSHIFTV352"
#define PRLAMPPDC_PHSHIFTV704String				"PRLAMPPDC_PHSHIFTV704"

#define PRLAMPPDC_352_InPowerString		"PRLAMPPDC_352_INPWR"
#define PRLAMPPDC_704_InPowerString		"PRLAMPPDC_704_INPWR"
#define PRLAMPPDC_352_RefPowerString	"PRLAMPPDC_352_REFPWR"
#define PRLAMPPDC_704_RefPowerString	"PRLAMPPDC_704_REFPWR"
#define PRLAMPPDC_352_OutPowerString	"PRLAMPPDC_352_OUTPWR"
#define PRLAMPPDC_704_OutPowerString	"PRLAMPPDC_704_OUTPWR"
#define PRLAMPPDC_352_TempString		"PRLAMPPDC_352_TEMP"
#define PRLAMPPDC_704_TempString		"PRLAMPPDC_704_TEMP"
#define PRLAMPPDC_352_TempStatString		"PRLAMPPDC_352_TEMPSTAT"
#define PRLAMPPDC_704_TempStatString		"PRLAMPPDC_704_TEMPSTAT"
#define PRLAMPPDC_352_AmpStatString		"PRLAMPPDC_352_AMPSTAT"
#define PRLAMPPDC_704_AmpStatString		"PRLAMPPDC_704_AMPSTAT"
#define PRLAMPPDC_352_OnOffString		"PRLAMPPDC_352_ONOFF"
#define PRLAMPPDC_704_OnOffString		"PRLAMPPDC_704_ONOFF"

class PRL_Amp_PDC : public asynPortDriver{
public:
    PRL_Amp_PDC(const char *portName, const char *ipPortSPI,
        const char *ipPortI2C, int devAddrI2C, int priority, 
        int stackSize);

	virtual ~PRL_Amp_PDC();

    virtual asynStatus readFloat64(asynUser *pasynUser, epicsFloat64 *value);
    virtual asynStatus readInt32(asynUser *pasynUser, epicsInt32 *value);
    virtual asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);
    virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);

protected:
	virtual asynStatus ipPortWrite(double timeou, asynUser *mAsynUserCommandt);
	virtual asynStatus ipPortRead(double timeout, asynUser *mAsynUserCommand);
	virtual asynStatus ipPortWriteRead(double timeout, asynUser *mAsynUserCommand);

    //I2C methods
    char const *status2MsgI2C(unsigned char status, unsigned char code);
    asynStatus packI2C(unsigned char type, unsigned char devAddr,
		unsigned char addrWidth, unsigned char *data, unsigned short len,
		unsigned int off);
    asynStatus unpackI2C(unsigned char type, unsigned char *data,
		unsigned short *len, asynStatus status);
    asynStatus doXferI2C(int asynAddr, unsigned char type, unsigned char devAddr,
		unsigned char addrWidth, unsigned char *data, unsigned short *len,
		unsigned int off, double timeout = 0.3);
    asynStatus xferI2C(int asynAddr, unsigned char type, unsigned char devAddr,
		unsigned char addrWidth, unsigned char *data, unsigned short *len,
		unsigned int off, double timeout = 0.3);
    asynStatus setMuxBusI2C(int asynAddr, int devAddr);

    //SPI methods
	asynStatus packSPI(unsigned char type, unsigned char *data, unsigned short len);
	asynStatus unpackSPI(unsigned char type, unsigned char *data,
		unsigned short *len, asynStatus status);
	asynStatus xferSPI(int asynAddr, unsigned char type, unsigned char *data,
			unsigned short *len, double timeout = 0.3);


	char mReq[AK_MAX_MSG_SZ];
	size_t mReqSz;
	size_t mReqActSz;
	char mResp[AK_MAX_MSG_SZ];
	size_t mRespSz;
	size_t mRespActSz;
	char mStatusMsg[AK_MAX_MSG_SZ];

	int StatusMessage;
    int PRLAmpPDC_PD0;
    int PRLAmpPDC_PD1;
    int PRLAmpPDC_PD2;
    int PRLAmpPDC_PD3;
    int PRLAmpPDC_PD4;

    int PRLAmpPDC_PD0PWR;
    int PRLAmpPDC_PD1PWR;
    int PRLAmpPDC_PD2PWR;
    int PRLAmpPDC_PD3PWR;
    int PRLAmpPDC_PD4PWR;

    int PRLAmpPDC_PWD0;
    int PRLAmpPDC_PWD1;
    int PRLAmpPDC_PWD2;
    int PRLAmpPDC_PWD3;

    int PRLAmpPDC_TMP;
    int PRLAmpPDC_TMPSP;
    int PRLAmpPDC_HUM;

    int PRLAmpPDC_PWRPID352;
    int PRLAmpPDC_PWRPID704;

    int PRLAmpPDC_RECAL;
    int PRLAmpPDC_COMP;
    int PRLAmpPDC_COMP_CAB;
    int PRLAmpPDC_COMP_AMP;
    int PRLAmpPDC_PHERR352;
    int PRLAmpPDC_PHERR704;

    int PRLAmpPDC_ATTVOLT352;
    int PRLAmpPDC_ATTVOLT704;
    int PRLAmpPDC_PHSHIFTV352;
    int PRLAmpPDC_PHSHIFTV704;
  
    int PRLAmpPDC_352_InPwr;
    int PRLAmpPDC_704_InPwr;
    int PRLAmpPDC_352_RefPwr;
    int PRLAmpPDC_704_RefPwr;
    int PRLAmpPDC_352_OutPwr;
    int PRLAmpPDC_704_OutPwr;
    int PRLAmpPDC_352_Temp;
    int PRLAmpPDC_704_Temp;
    int PRLAmpPDC_352_TempStat;
    int PRLAmpPDC_704_TempStat;
    int PRLAmpPDC_352_AmpStat;
    int PRLAmpPDC_704_AmpStat;
    int PRLAmpPDC_352_OnOff;
    int PRLAmpPDC_704_OnOff;

private:
	void hexdump(void *mem, unsigned int len);
	asynStatus readI2C(int addr, unsigned char reg, unsigned short *val, unsigned short len);
    asynStatus writeI2C(int addr, unsigned char reg, unsigned char *val, unsigned short len);
    asynStatus selectChipSPI(int addr, int chip);
    
    //PDC Methods
	asynStatus readPD(int addr, double *val, int reg_addr);
    asynStatus readPWD(int addr, double *value, int reg_addr);
    asynStatus readTemp(int addr, double *value, int reg_addr);
    asynStatus readRecalib(int addr, int *value);
    asynStatus readPowerPID(int addr, double *value, int reg_addr);
    asynStatus readComp(int addr, int *value);
    asynStatus readHumidity(int addr, int *value);
    asynStatus writeTemp(int addr, double val);
    asynStatus writePowerPID(int addr, double val, int reg_addr);
    asynStatus writeRecalib(int addr, int val);
    asynStatus writeComp(int addr, int val);
    asynStatus readPhErr(int addr, double *value, int reg_addr);
    asynStatus readPDCVoltage(int addr, double *value, int reg_addr);

    //Amplifiers methods
    asynStatus readVoltage(int addr, double * value, int readCode, int amp);
    asynStatus readStatus(int addr, int * value, int flag, int amp);
    asynStatus setOnOff(int addr, int value, int amp);

    int addrI2C;
	char *mIpPortSPI;
	char *mIpPortI2C;

	asynUser *mAsynUserSPI;
	asynUser *mAsynUserI2C;
	static I2CMuxInfo mMuxInfo[AK_I2C_MUX_MAX];
};

#endif /* _PRL_Amp_PDC_H_ */
