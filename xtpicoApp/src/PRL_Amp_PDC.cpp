/*
 * PRL_Amp_PDC.cpp
 *
 *  Created on: Mar 3, 2021
 *      Author: gabrielfedel
 */


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>
#include <ctype.h>

#include <epicsTypes.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsString.h>
#include <epicsTimer.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <epicsExit.h>
#include <epicsExport.h>
#include <iocsh.h>

#include <asynPortDriver.h>
#include "PRL_Amp_PDC.h"

static const char *driverName = "PRL_Amp_PDC";

static void exitHandler(void *drvPvt) {
	PRL_Amp_PDC *pPvt = (PRL_Amp_PDC *)drvPvt;
	delete pPvt;
}

//// Copied from AKBase -- Begin

void PRL_Amp_PDC::hexdump(void *mem, unsigned int len) {
	unsigned int i, j;

	for (i = 0;	i < len + ((len % HEXDUMP_COLS) ?
							(HEXDUMP_COLS - len % HEXDUMP_COLS) : 0); i++) {
		/* print offset */
		if (i % HEXDUMP_COLS == 0) {
			printf("0x%06x: ", i);
		}

		/* print hex data */
		if (i < len) {
			printf("%02x ", 0xFF & ((char*) mem)[i]);
		} else {
			/* end of block, just aligning for ASCII dump */
			printf("   ");
		}

		/* print ASCII dump */
		if (i % HEXDUMP_COLS == (HEXDUMP_COLS - 1)) {
			for (j = i - (HEXDUMP_COLS - 1); j <= i; j++) {
				if (j >= len) {
					/* end of block, not really printing */
					putchar(' ');
				} else if (isprint(((char*) mem)[j])) {
					/* printable char */
					putchar(0xFF & ((char*) mem)[j]);
				} else {
					/* other char */
					putchar('.');
				}
			}
			putchar('\n');
		}
	}
}

asynStatus PRL_Amp_PDC::ipPortWriteRead(double timeout, asynUser *mAsynUserCommand) {
	int eomReason = 0;
	asynStatus status = asynSuccess;
	const char *functionName="ipPortWriteRead";

	D(printf("request mReqSz = %ld, timeout = %f:\n", mReqSz, timeout));
	D0(hexdump(mReq, mReqSz));

	// A MUST!
	// delay 1 ms before next access otherwise the requests to the XT pico
	// happen too fast for and they might not be handled properly!
	usleep(1000);

	status = pasynOctetSyncIO->writeRead(mAsynUserCommand,
		mReq, mReqSz, mResp, mRespSz,
		timeout, &mReqActSz, &mRespActSz, &eomReason);

	D(printf("response mRespSz = %ld, mRespActSz = %ld, eomReason = %d:\n", mRespSz, mRespActSz, eomReason));
	D0(hexdump(mResp, mRespActSz));

	if (status) {
		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR,
			"%s:%s, status=%d, eomReason %d\n", driverName, functionName, status, eomReason);
	}

	return status;
}

asynStatus PRL_Amp_PDC::ipPortWrite(double timeout, asynUser *mAsynUserCommand) {
	asynStatus status = asynSuccess;
	const char *functionName="ipPortWrite";

	D(printf("request (%ld bytes):\n", mReqSz));
	D0(hexdump(mReq, mReqSz));

	// A MUST!
	// delay 1 ms before next access otherwise the requests to the XT pico
	// happen too fast for and they might not be handled properly!
	usleep(4000);

	status = pasynOctetSyncIO->write(mAsynUserCommand,
		mReq, mReqSz, timeout, &mReqActSz);

	D(printf("request actual size %ld bytes\n", mReqActSz));
	if (status) {
		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR,
			"%s:%s, status=%d\n", driverName, functionName, status);
	}

	return status;
}

asynStatus PRL_Amp_PDC::ipPortRead(double timeout, asynUser *mAsynUserCommand) {
	int eomReason = 0;
	asynStatus status = asynSuccess;
	const char *functionName = "ipPortRead";

	// A MUST!
	// delay 1 ms before next access otherwise the requests to the XT pico
	// happen too fast for and they might not be handled properly!
	usleep(4000);

	status = pasynOctetSyncIO->read(mAsynUserCommand,
		mResp, mRespSz, timeout, &mRespActSz, &eomReason);

	D(printf("response mRespSz = %ld, mRespActSz = %ld, eomReason = %d:\n", mRespSz, mRespActSz, eomReason));
	D0(hexdump(mResp, mRespActSz));

	if (status) {
		asynPrint(this->pasynUserSelf, ASYN_TRACE_ERROR,
			"%s:%s, status=%d, eomReason %d\n", driverName, functionName, status, eomReason);
	}

	return status;
}
//// Copied from AKBase -- End



//// Copied from AKI2C -- Beging


I2CMuxInfo PRL_Amp_PDC::mMuxInfo[AK_I2C_MUX_MAX] = {
		{-1, -1},
		{-1, -1},
		{-1, -1},
		{-1, -1},
		{-1, -1},
		{-1, -1},
		{-1, -1},
		{-1, -1}
};

/*
 *
 * See design_guide_sxl_en.pdf, page 51.
 *
WRITE: 0x02,0x00,0x0A,0x03,0x50,0x02,0x00,0x00,0x57,0x00,0x01,0xnn,0x03
BREAK DOWN:
	0x02		STX
	0x00,0x0A	Len (10 Bytes) follows, always 2 bytes long
	0x03		function code (with all messages)
	0x50		Slave Address
	0x02		Count Internal Address
	0x00,0x00	Internal Address 0x00,0x00, Count(0-4 Byte)
	0x57		W = WRITE
	0x00,0x01	write 1 byte, always 2 bytes
	0xnn		Byte to write
	0x03		ETX


READ: 0x02,0x00,0x09,0x03,0x50,0x02,0x00,0x00,0x52,0x00,0x02,0x03
BREAK DOWN:
	0x02		STX
	0x00,0x09	Len (9 Bytes) follows, always 2 bytes long
	0x03		function code (with all messages)
	0x50		Slave Address
	0x02		Count Internal Address
	0x00,0x00	Internal Address 0x00,0x00, Count(0-4 Byte)
	0x52		R = READ
	0x00,0x02	read 2 byte, always 2 bytes
	0x03		ETX

Responses:

	MSG: (relates to the 'function code' above)
		no ACK/NAK			(function code = 0x00)
		NAK only			(function code = 0x01)
		ACK only			(function code = 0x02)
		ACK and NAK			(function code = 0x03)

	NAK
		0x15,'S'			NAK STX
		0x15,'E'			NAK ETX
		0x15,'A'			NAK Slave Address
		0x15,'C'			NAK Command
		0x15,'L'			NAK Len
		0x15,'B'			NAK Buffer
		0x15,'R',..			NAK Read and Data we could read
		0x15,'W',nn,nn		NAK Write and nn,nn = Data we could write

	ACK
		0x06,'R'...			ACK Read and Data
		0x06,'W'			ACK Write

*/

char const *PRL_Amp_PDC::status2MsgI2C(unsigned char status, unsigned char code) {
	if (status == AK_I2C_STATUS_OK) {
		switch(code) {
		case 'R':
			return "Read OK";
			break;
		case 'W':
			return "Write OK";
			break;
		default:
			return "Unknown OK code";
			break;
		}
	} else if (status == AK_I2C_STATUS_FAIL) {
		switch(code) {
		case 'R':
			return "Read error";
			break;
		case 'W':
			return "Write error";
			break;
		case 'S':
			return "STX error";
			break;
		case 'E':
			return "ETX error";
			break;
		case 'A':
			return "Slave address error";
			break;
		case 'C':
			return "Command error";
			break;
		case 'L':
			return "Length error";
			break;
		case 'B':
			return "Buffer error";
			break;
		default:
			return "Unknown FAIL code";
			break;
		}
	} else {
		return "Unknown status";
	}

	return "Unknown!";
}

asynStatus PRL_Amp_PDC::packI2C(unsigned char type, unsigned char devAddr,
		unsigned char addrWidth, unsigned char *data, unsigned short len,
		unsigned int off) {
	asynStatus status = asynSuccess;
	int i, l, msgLen;
	unsigned char msg[AK_MAX_MSG_SZ] = {0};
	l = 0;

	if (devAddr > 0x7F) {
		sprintf(mStatusMsg, "Invalid I2C address");
		return asynError;
	}
	if (addrWidth > 4) {
		sprintf(mStatusMsg, "Invalid I2C address width");
		return asynError;
	}

	// calculate the request length wo/ STX and length included (3 bytes)
	msgLen =  1							// function code
			+ 1							// slave address
			+ 1							// count internal address
			+ addrWidth					// internal address width (0 - 4)
			+ 1							// W - WRITE, R - READ
			+ 2							// bytes to write
			+ 1;						// ETX
	if (type == AK_REQ_TYPE_WRITE) {
		msgLen += len;					// data length
	}
	if (msgLen > AK_MAX_MSG_SZ) {
		sprintf(mStatusMsg, "Invalid I2C request length");
		return asynError;
	}
	if ((type == AK_REQ_TYPE_READ) && (len + 2 > AK_MAX_MSG_SZ)) {
		sprintf(mStatusMsg, "Invalid I2C response length");
		return asynError;
	}

	// build the request
	msg[l++] = 0x02;					// STX
	msg[l++] = (msgLen >> 8) & 0xFF;	// high byte of message length that follows
	msg[l++] = (msgLen & 0xFF);			// high byte of message length that follows
	msg[l++] = 0x03;					// function code
	msg[l++] = devAddr;					// slave address
	msg[l++] = addrWidth;				// count internal address
	// add specified intAddrWidth amount of bytes to the message
	switch(addrWidth) {
	case 0:
		// no additional bytes
		break;
	case 1:
		msg[l++] = off & 0xFF;
		break;
	case 2:
		msg[l++] = (off >> 8) & 0xFF;
		msg[l++] = off & 0xFF;
		break;
	case 3:
		msg[l++] = (off >> 16) & 0xFF;
		msg[l++] = (off >> 8) & 0xFF;
		msg[l++] = off & 0xFF;
		break;
	case 4:
		msg[l++] = (off >> 24) & 0xFF;
		msg[l++] = (off >> 16) & 0xFF;
		msg[l++] = (off >> 8) & 0xFF;
		msg[l++] = off & 0xFF;
		break;
	}
	msg[l++] = type;					// W - WRITE, R - READ
	msg[l++] = (len >> 8) & 0xFF;		// high byte count to write
	msg[l++] = len & 0xFF;				// low byte count to write
	if (type == AK_REQ_TYPE_WRITE) {
		for (i = 0; i < len; i++) {
			msg[l++] = *(data + i);		// data byte[i]
		}
	}
	msg[l++] = 0x03;					// ETX

	memset(mReq, 0, AK_MAX_MSG_SZ);
	memset(mResp, 0, AK_MAX_MSG_SZ);
	mReqActSz = 0;
	mRespActSz = 0;
	mReqSz = l;
	// maximum bytes received:
	if (type == AK_REQ_TYPE_WRITE) {
		// ACK: two status bytes received
		// NAK: we will try to read out any outstanding data and discard it
		mRespSz = 2;
	} else if (type == AK_REQ_TYPE_READ) {
		// ACK: two status bytes + data
		// NAK: we will try to read out any outstanding data and discard it
		mRespSz = len + 2;
	}

	memcpy(mReq, msg, mReqSz);

	return status;
}

asynStatus PRL_Amp_PDC::unpackI2C(unsigned char type, unsigned char *data,
		unsigned short *len, asynStatus status) {

	if (mRespActSz >= 2) {
		if (mResp[0] == AK_I2C_STATUS_FAIL) {
			E(printf("FAIL - we got NAK! %02X %02X\n", mResp[0], mResp[1]));
			*len = 0;
		} else if (mResp[0] == AK_I2C_STATUS_OK) {
			D(printf("OK  - we got ACK! %02X %02X\n", mResp[0], mResp[1]));
			if ((type == AK_REQ_TYPE_READ) && data && len) {
				memcpy(data, &mResp[AK_I2C_RESP_HDR_SZ],
					*len * sizeof(unsigned char));
			}
			status = asynSuccess;
		} else {
			E(printf("FAIL - unknown response code! %02X %02X\n",
				mResp[0], mResp[1]));
		}
		sprintf(mStatusMsg, "%s", status2MsgI2C(mResp[0], mResp[1]));
	} else {
		sprintf(mStatusMsg, "Invalid I2C response size received");
	}

	return status;
}

asynStatus PRL_Amp_PDC::doXferI2C(int asynAddr, unsigned char type, unsigned char devAddr,
		unsigned char addrWidth, unsigned char *data, unsigned short *len,
		unsigned int off, double timeout) {
	asynStatus status = asynSuccess;

	if ((type != AK_REQ_TYPE_WRITE) && (type != AK_REQ_TYPE_READ)) {
		sprintf(mStatusMsg, "Invalid request type");
		status = asynError;
	}

	if (status == asynSuccess) {
		status = packI2C(type, devAddr, addrWidth, data, *len, off);
	}
	if (status == asynSuccess) {
		if (data) {
			status = ipPortWriteRead(timeout, mAsynUserI2C); // PDC communication
			// status is checked in the unpack() below
			status = unpackI2C(type, data, len, status);
		} else {
			// this is to read out any outstanding data that might be left
			// after receiving NAK (on either read or write access)
			return ipPortRead(timeout, mAsynUserI2C);
		}
	}

	if (status) {
		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR,
			"%s::%s, status=%d\n",
			driverName, __func__, status);
	}

	setStringParam(asynAddr, StatusMessage, mStatusMsg);
	char m[AK_MAX_MSG_SZ] = {0};
	getStringParam(asynAddr, StatusMessage, AK_MAX_MSG_SZ, m);
	D(printf("Status message: '%s'\n", m));

	/* Do callbacks so higher layers see any changes */
	callParamCallbacks(asynAddr, asynAddr);

	return status;
}

asynStatus PRL_Amp_PDC::xferI2C(int asynAddr, unsigned char type, unsigned char devAddr,
		unsigned char addrWidth, unsigned char *data, unsigned short *len,
		unsigned int off, double timeout) {
	asynStatus status = asynSuccess;

	status = setMuxBusI2C(asynAddr, devAddr);
	if (status) {
		return status;
	}

	status = doXferI2C(asynAddr, type, devAddr, addrWidth, data, len, off, timeout);
	if (status == asynSuccess) {
		return status;
	}

	/* there could be some data left to read out even if NAK was returned,
	 * read & discard */
	unsigned short tmplen = AK_MAX_MSG_SZ - 2;
	doXferI2C(asynAddr, AK_REQ_TYPE_READ, devAddr, addrWidth, NULL, &tmplen, 0, timeout);

	/* return error status of the previous access */
	return status;
}

asynStatus PRL_Amp_PDC::setMuxBusI2C(int asynAddr, int devAddr) {
	unsigned char data;
	unsigned short len;
	int muxAddr = 0;
	int muxBus = 0;
	asynStatus status = asynSuccess;


	D(printf("DEV 0x%02X WANTS MUX 0x%02X : %d\n", devAddr, muxAddr, muxBus));

	/* No MUX bus to setup */
	if (muxAddr == 0) {
		D(printf("MUX setup NOT required (MUX addr == 0)\n"));
		return asynSuccess;
	}

	D(printf("Known MUXes:\n"));
	for (int i = 0; i < AK_I2C_MUX_MAX; i++) {
		if (mMuxInfo[i].addr != -1) {
			D(printf("[%d] MUX 0x%02X : %d\n",
				i, mMuxInfo[i].addr, mMuxInfo[i].bus));
		}
	}

	/* Do we need to setup MUX bus? */
	for (int i = 0; i < AK_I2C_MUX_MAX; i++) {
		if ((mMuxInfo[i].addr == muxAddr) && (mMuxInfo[i].bus == muxBus)) {
			/* MUX bus already setup */
			D(printf("DEV 0x%02X ALREADY USING MUX 0x%02X : %d\n",
				devAddr, muxAddr, muxBus));
			return asynSuccess;
		}
	}

	/* We need to change the MUX bus */
	data = (1 << muxBus);
	len = 1;
	status = doXferI2C(asynAddr, AK_REQ_TYPE_WRITE, muxAddr, 0, &data, &len, 0);
	if (status) {
		return status;
	}

	/* Update the currently selected MUX bus */
	for (int i = 0; i < AK_I2C_MUX_MAX; i++) {
		if (mMuxInfo[i].addr == muxAddr) {
			mMuxInfo[i].bus = muxBus;
			D(printf("[%d] UPDATED MUX 0x%02X : %d\n",
				i, mMuxInfo[i].addr, mMuxInfo[i].bus));
		} else {
			if (mMuxInfo[i].addr != -1) {
				D(printf("[%d] MUX 0x%02X : %d\n",
					i, mMuxInfo[i].addr, mMuxInfo[i].bus));
			}
		}
	}

	D(printf("DEV 0x%02X NOW USING MUX 0x%02X : %d\n",
		devAddr, muxAddr, muxBus));

	return status;
}

//// Copied from AKI2C -- End


asynStatus PRL_Amp_PDC::writeI2C(int addr, unsigned char reg, unsigned char *val, unsigned short len) {
	asynStatus status = asynSuccess;

	status = xferI2C(addr, AK_REQ_TYPE_WRITE, addrI2C, 1, val, &len, reg);
	if (status) {
		return status;
	}

	return status;
}


asynStatus PRL_Amp_PDC::writeTemp(int addr, double val) {
	asynStatus status = asynSuccess;
    long int intVal;
	unsigned char regVal[4] = {0};


    intVal = (long int)(val*1000000);
    for (int i=0; i < 4 ;i++) {
        regVal[i] = (intVal >> (8 * i)) & 0xFF;
    }

	status = writeI2C(addr, PRLAMPPDC_TMP_SP_REG, regVal, 4);
	if (status) {
		return status;
	}

	return status;
}


asynStatus PRL_Amp_PDC::writeRecalib(int addr, int val) {
	asynStatus status = asynSuccess;
	unsigned char regVal[1] = {0};

    // Invalid value
    if (val != 0 && val != 1)
        return asynError;

    status = writeI2C(addr, PRLAMPPDC_RECAL_REG, regVal, 1);
	if (status) {
		return status;
	}

	return status;
}

asynStatus PRL_Amp_PDC::writePowerPID(int addr, double val, int reg_addr) {
	asynStatus status = asynSuccess;
    long int intVal;
	unsigned char regVal[2] = {0};


    // Invalid interval
    if (val > 0 || val < -65.536)
        return asynError;

    intVal = (long int)(val*-1000);

    for (int i=0; i < 2 ;i++) {
        regVal[i] = (intVal >> (8 * i)) & 0xFF;
        printf("regVAl[%d] %02x\n", i, regVal[i] );
    }

	status = writeI2C(addr, reg_addr, regVal, 2);
	if (status) {
		return status;
	}

	return status;
}

asynStatus PRL_Amp_PDC::writeComp(int addr, int val) {
	asynStatus status = asynSuccess;
	unsigned char regVal[1] = {0};

    if (val != 1 and val != 4)
        return asynError;

    regVal[0] = val;

	status = writeI2C(addr, PRLAMPPDC_COMP_REG, regVal, 1);
	if (status) {
		return status;
	}

	return status;
}


asynStatus PRL_Amp_PDC::readI2C(int addr, unsigned char reg, unsigned short *val, unsigned short len) {
	asynStatus status = asynSuccess;
	unsigned char data[len];

    // Initialize data array
    for (int i = 0; i < len; i++){
        data[i] = 0;
    }

	/* Read register - config, status and ID are 1 byte long, others 2 bytes */
	status = xferI2C(addr, AK_REQ_TYPE_READ, addrI2C, 1, data, &len, reg);
	if (status) {
		return status;
	}
    for (int i =0; i < len; i++) {
        val[i] = data[i];
    }
//	D(printf("reg %d, READ value 0x%04X\n", reg, *val));

    //If needed uncomment this 
	//usleep(1000);
	return status;
}

asynStatus PRL_Amp_PDC::readPD(int addr, double *value, int reg_addr) {
	asynStatus status = asynSuccess;
	unsigned short regVal[5];
	double pd;

	status = readI2C(addr, reg_addr, regVal, 5);
	if (status) {
		return status;
	}

    pd = 0;
    for (int i =0; i < 5; i++) {
        pd += regVal[i] << i*8;
    }

    *value = pd*0.001;

	return status;
}

asynStatus PRL_Amp_PDC::readPWD(int addr, double *value, int reg_addr) {
	asynStatus status = asynSuccess;
	unsigned short regVal[8];
	double pwd;

	status = readI2C(addr, reg_addr, regVal, 8);
	if (status) {
		return status;
	}

    pwd = 0;
    for (int i =0; i < 8; i++) {
        pwd += regVal[i] << i*8;
    }

    *value = pwd*0.001;

	return status;
}

asynStatus PRL_Amp_PDC::readTemp(int addr, double *value, int reg_addr) {
	asynStatus status = asynSuccess;
	unsigned short regVal[4];
	double tmp;

	status = readI2C(addr, reg_addr, regVal, 4);
	if (status) {
		return status;
	}

    tmp = 0;
    for (int i =0; i < 4; i++) {
        tmp += regVal[i] << i*8;
    }

    *value = tmp*0.000001;

	return status;
}

asynStatus PRL_Amp_PDC::readRecalib(int addr, int *value) {
	asynStatus status = asynSuccess;
	unsigned short regVal[1];
	int recalib_status;

	status = readI2C(addr, PRLAMPPDC_RECAL_REG, regVal, 1);
	if (status) {
		return status;
	}

    recalib_status = (regVal[0] & 0xF0) >> 4;

    *value = recalib_status;

	return status;
}

asynStatus PRL_Amp_PDC::readComp(int addr, int *value) {
	asynStatus status = asynSuccess;
	unsigned short regVal[1];

	status = readI2C(addr, PRLAMPPDC_COMP_REG, regVal, 1);
	if (status) {
		return status;
	}

		switch(regVal[0]) {
		case 0x1:
			*value = 0;
			break;
		case 0x2:
			*value = 1;
			break;
		case 0x4:
			*value = 0;
			break;
		case 0x8:
			*value = 2;
			break;
		default: // invalid value
			*value = 3;
            return asynError;
		}

	return status;
}

asynStatus PRL_Amp_PDC::readHumidity(int addr, int *value) {
	asynStatus status = asynSuccess;
	unsigned short regVal[1];

	status = readI2C(addr, PRLAMPPDC_HUM_REG, regVal, 1);
	if (status) {
		return status;
	}
	*value = regVal[0];
	return status;
}

asynStatus PRL_Amp_PDC::readPhErr(int addr, double *value, int reg_addr) {
	asynStatus status = asynSuccess;
	unsigned short regVal[2];
	double pherr;
	double neg;

	status = readI2C(addr, reg_addr, regVal, 2);
	if (status) {
		return status;
	}
    neg = (regVal[1]&128)>>7; // gets the sign
    
	pherr = (regVal[0]+(regVal[1]<<8)-128*neg)*pow(-1, neg);

    *value = pherr;

	return status;
}

asynStatus PRL_Amp_PDC::readPDCVoltage(int addr, double *value, int reg_addr) {
	asynStatus status = asynSuccess;
	unsigned short regVal[2];
	double voltage;

	status = readI2C(addr, reg_addr, regVal, 2);
	if (status) {
		return status;
	}

    voltage = 0;
    for (int i =0; i < 2; i++) {
        voltage += regVal[i] << i*8;
    }

    *value = voltage;

	return status;
}

asynStatus PRL_Amp_PDC::readPowerPID(int addr, double *value, int reg_addr) {
	asynStatus status = asynSuccess;
	unsigned short regVal[2];
	double pid;

	status = readI2C(addr, reg_addr, regVal, 2);
	if (status) {
		return status;
	}

    pid = 0;
    for (int i =0; i < 2; i++) {
        pid += regVal[i] << i*8;
    }

    *value = -pid*0.001;

	return status;
}

/////Copied from AKSPI -- Begin


asynStatus PRL_Amp_PDC::packSPI(unsigned char type, unsigned char *data,
		unsigned short len) {
	asynStatus status = asynSuccess;
	unsigned char msg[AK_MAX_MSG_SZ] = {0};
	int i;
	int l = 0;

	// build the request
	for (i = 0; i < len; i++) {
		msg[l++] = *(data + i);		// data byte[i]
	}

	memset(mReq, 0, AK_MAX_MSG_SZ);
	memset(mResp, 0, AK_MAX_MSG_SZ);
	mReqActSz = 0;
	mRespActSz = 0;
	mReqSz = l;
	// maximum bytes received depend on the operation
	if (type == AK_REQ_TYPE_WRITE) {
		mRespSz = 0;
	} else if (type == AK_REQ_TYPE_READ) {
		mRespSz = len;
	}

	memcpy(mReq, msg, mReqSz);

	return status;
}

asynStatus PRL_Amp_PDC::unpackSPI(unsigned char type, unsigned char *data,
		unsigned short *len, asynStatus status) {

	if (mRespActSz == *len) {
		D(printf("OK!\n"));
		if ((type == AK_REQ_TYPE_READ) && data && len) {
			memcpy(data, &mResp, *len * sizeof(unsigned char));
		}
		status = asynSuccess;
		sprintf(mStatusMsg, "OK");
	} else {
		sprintf(mStatusMsg, "Invalid SPI response size received");
	}

	return status;
}

asynStatus PRL_Amp_PDC::xferSPI(int asynAddr, unsigned char type, unsigned char *data,
		unsigned short *len, double timeout) {
	asynStatus status = asynSuccess;

	if ((type != AK_REQ_TYPE_WRITE) && (type != AK_REQ_TYPE_READ)) {
		sprintf(mStatusMsg, "Invalid request type");
		status = asynError;
	}

	if (status == asynSuccess) {
		status = packSPI(type, data, *len);
	}
	if (status == asynSuccess) {
		if (type == AK_REQ_TYPE_WRITE) {
			/* No data is returned by the remote host */
			status = ipPortWrite(timeout, mAsynUserSPI);
		} else {
			/* One or more bytes are returned by the remote host */
			status = ipPortWriteRead(timeout, mAsynUserSPI);
			status = unpackSPI(type, data, len, status);
		}
	}

	if (status) {
		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR,
			"%s::%s, status=%d\n",
			driverName, __func__, status);
	}

	//setStringParam(asynAddr, AKStatusMessage, mStatusMsg);
	char m[AK_MAX_MSG_SZ] = {0};
	//getStringParam(asynAddr, AKStatusMessage, AK_MAX_MSG_SZ, m);
	D(printf("Status message: '%s'\n", m));

	/* Do callbacks so higher layers see any changes */
	callParamCallbacks(asynAddr, asynAddr);

	/* return error status of the previous access */
	return status;
}

// Should set a bit on I2C to select which chip will be used
asynStatus PRL_Amp_PDC::selectChipSPI(int addr, int chip) {
    unsigned char data[1];
    data[0] = chip;
    return writeI2C(addr, PRLAMPPDC_CS_REG, data, 1);
}

/////Copied from AKSPI -- End


//Method to read Status
//flag == 0 : temperature status
//flag == 1 : amplifier status
asynStatus PRL_Amp_PDC::readStatus(int addr, int * value, int flag, int amp) {
	asynStatus status = asynSuccess;
	unsigned char data[1] = {0};

	unsigned short len = 1;
    
    data[0] = PRLAMPPDC_Status;

    //Should lock because will use the I2C to select the chip
    lock();

    status =  selectChipSPI(addr, amp);

	if (status) {
        unlock();
		return status;
	}

	status = xferSPI(addr, AK_REQ_TYPE_READ, data, &len);
	if (status) {
        unlock();
		return status;
	}

	data[0] = 0;
	status = xferSPI(addr, AK_REQ_TYPE_READ, data, &len);

	if (status) {
        unlock();
		return status;
	}

    unlock();

    *value = (data[0] >> flag) & 1;

	return status;
}

//Method to read Voltage representing InputPower, Reflected Power, 
//Output power or temperature
//amp: 0: 352, 1: 704
asynStatus PRL_Amp_PDC::readVoltage(int addr, double * value, int readCode, int amp) {
	asynStatus status = asynSuccess;
	
	unsigned char data[4] = {0};

	unsigned short len = 1;// send command first
    
    data[0] = readCode;

    //Should lock because will use the I2C to select the chip
    lock();

    status =  selectChipSPI(addr, amp);

	if (status) {
        unlock();
		return status;
	}

	status = xferSPI(addr, AK_REQ_TYPE_READ, data, &len);
	if (status) {
        unlock();
		return status;
	}
	data[0] = 0;
	len = 4; //read data
	status = xferSPI(addr, AK_REQ_TYPE_READ, data, &len);
    unlock();

	if (status) {
		return status;
	}

    *value = (data[0] << 24 | data[1] << 16 | data[2] << 8 | data[3])*1.2*2.75/268435456;
	D(printf("Returned data %f \n",*value));
	return status;
}

asynStatus PRL_Amp_PDC::setOnOff(int addr, int value, int amp) {
	asynStatus status = asynSuccess;
	unsigned char data[1] = {0};

	unsigned short len = 1;
    

    //Should lock because will use the I2C to select the chip
    lock();

    status =  selectChipSPI(addr, amp);

	if (status) {
        unlock();
		return status;
	}


    if (value == 1)
        data[0] = PRLAMPPDC_OnCode;
    else
        data[0] = PRLAMPPDC_OffCode;

    status = xferSPI(addr, AK_REQ_TYPE_WRITE, data, &len);
    if (status) {
        unlock();
        return status;
    }

    unlock();
	return status;
}


asynStatus PRL_Amp_PDC::readFloat64(asynUser *pasynUser, epicsFloat64 *value) {
	int function = pasynUser->reason;
	int addr = 0;
	asynStatus status = asynSuccess;
	const char *functionName = "readFloat64";
	const char *paramName ;
	getParamName(function,&paramName);
	status = getAddress(pasynUser, &addr);
	if (status != asynSuccess) {
		return(status);
	}

    // PDC values
	if (function == PRLAmpPDC_PD0) {
		status = readPD(addr, value, PRLAMPPDC_PD0_REG);
    }  else	if (function == PRLAmpPDC_PD1) {
		status = readPD(addr, value, PRLAMPPDC_PD1_REG);
    }  else if (function == PRLAmpPDC_PD2) {
		status = readPD(addr, value, PRLAMPPDC_PD2_REG);
    }  else	if (function == PRLAmpPDC_PD3) {
		status = readPD(addr, value, PRLAMPPDC_PD3_REG);
    }  else	if (function == PRLAmpPDC_PD4) {
		status = readPD(addr, value, PRLAMPPDC_PD4_REG);
    } if (function == PRLAmpPDC_PD0PWR) {
		status = readPD(addr, value, PRLAMPPDC_PD0PWR_REG);
    } else if (function == PRLAmpPDC_PD1PWR) {
		status = readPD(addr, value, PRLAMPPDC_PD1PWR_REG);
    } else if (function == PRLAmpPDC_PD2PWR) {
		status = readPD(addr, value, PRLAMPPDC_PD2PWR_REG);
    } else if (function == PRLAmpPDC_PD3PWR) {
		status = readPD(addr, value, PRLAMPPDC_PD3PWR_REG);
    } else if (function == PRLAmpPDC_PD4PWR) {
		status = readPD(addr, value, PRLAMPPDC_PD4PWR_REG);
    } else if (function == PRLAmpPDC_PWD0) {
		status = readPWD(addr, value, PRLAMPPDC_PWD0_REG);
    } else if (function == PRLAmpPDC_PWD1) {
		status = readPWD(addr, value, PRLAMPPDC_PWD1_REG);
    } else if (function == PRLAmpPDC_PWD2) {
		status = readPWD(addr, value, PRLAMPPDC_PWD2_REG);
    } else if (function == PRLAmpPDC_PWD3) {
		status = readPWD(addr, value, PRLAMPPDC_PWD3_REG);
    } else if (function == PRLAmpPDC_TMPSP) {
		status = readTemp(addr, value, PRLAMPPDC_TMP_SP_REG);
    } else if (function == PRLAmpPDC_TMP) {
		status = readTemp(addr, value, PRLAMPPDC_TMP_READ_REG);
    } else if (function == PRLAmpPDC_PWRPID352) {
		status = readPowerPID(addr, value, PRLAMPPDC_PWRPID352_REG);
    } else if (function == PRLAmpPDC_PWRPID704) {
		status = readPowerPID(addr, value, PRLAMPPDC_PWRPID704_REG);
    } else if (function == PRLAmpPDC_PHERR352) {
		status = readPhErr(addr, value, PRLAMPPDC_PHERR352_REG);
	} else if (function == PRLAmpPDC_PHERR704) {
		status = readPhErr(addr, value, PRLAMPPDC_PHERR704_REG);
	} else if (function == PRLAmpPDC_ATTVOLT352) {
		status = readPDCVoltage(addr, value, PRLAMPPDC_ATTVOLT352_REG);
	} else if (function == PRLAmpPDC_ATTVOLT704) {
		status = readPDCVoltage(addr, value, PRLAMPPDC_ATTVOLT704_REG);
	} else if (function == PRLAmpPDC_PHSHIFTV352) {
		status = readPDCVoltage(addr, value, PRLAMPPDC_PHSHIFTV352_REG);
	} else if (function == PRLAmpPDC_PHSHIFTV704) {
		status = readPDCVoltage(addr, value, PRLAMPPDC_PHSHIFTV704_REG);
	}
    //Amp values
    else if(function == PRLAmpPDC_352_InPwr) {
		status = readVoltage(addr, value, PRLAMPPDC_InPowerCode, 0);
    }
    else if(function == PRLAmpPDC_704_InPwr) {
		status = readVoltage(addr, value, PRLAMPPDC_InPowerCode, 1);
    } else if (function == PRLAmpPDC_352_RefPwr) {
		status = readVoltage(addr, value, PRLAMPPDC_RefPowerCode, 0);
    } else if (function == PRLAmpPDC_704_RefPwr) {
		status = readVoltage(addr, value, PRLAMPPDC_RefPowerCode, 1);
    } else if (function == PRLAmpPDC_352_OutPwr) {
		status = readVoltage(addr, value, PRLAMPPDC_OutPowerCode, 0);
    } else if (function == PRLAmpPDC_704_OutPwr) {
		status = readVoltage(addr, value, PRLAMPPDC_OutPowerCode, 1);
    } else if (function == PRLAmpPDC_352_Temp) {
		status = readVoltage(addr, value, PRLAMPPDC_TempCode, 0);
    } else if (function == PRLAmpPDC_704_Temp) {
		status = readVoltage(addr, value, PRLAMPPDC_TempCode, 1);
    }


	/* Do callbacks so higher layers see any changes */
	callParamCallbacks(addr, addr);

	if (status) {
		asynPrint(pasynUser, ASYN_TRACE_ERROR,
			"%s:%s: error, status=%d function=%d,paramName=%s, addr=%d, value=%f\n",
			driverName, functionName, status, function, paramName, addr, *value);
	} else {
		asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
			"%s:%s: function=%d, addr=%d, value=%f\n",
			driverName, functionName, function, addr, *value);
	}

	return status;
}


asynStatus PRL_Amp_PDC::readInt32(asynUser *pasynUser, epicsInt32 *value) {

	int function = pasynUser->reason;
	int addr = 0;
	asynStatus status = asynSuccess;
	const char *functionName = "readInt32";

	status = getAddress(pasynUser, &addr);
	if (status != asynSuccess) {
		return(status);
	}

	//status = setFloatParam(addr, function, value);

    //PDC
	if (function == PRLAmpPDC_RECAL) {
		status = readRecalib(addr, value);
    } else if (function == PRLAmpPDC_COMP) {
		status = readComp(addr, value);
	} else if (function == PRLAmpPDC_HUM) {
		status = readHumidity(addr, value);
    } else

    //AMP
	if (function == PRLAmpPDC_352_TempStat) {
		status = readStatus(addr, value, 0, 0); // flag 0
    } else if (function == PRLAmpPDC_704_TempStat) {
		status = readStatus(addr, value, 0, 1); // flag 0
    } else if (function == PRLAmpPDC_352_AmpStat) {
		status = readStatus(addr, value, 1, 0); // flag 1
    } else if (function == PRLAmpPDC_704_AmpStat) {
		status = readStatus(addr, value, 1, 1); // flag 1
    } 

	/* Do callbacks so higher layers see any changes */
	callParamCallbacks(addr, addr);

	if (status) {
		asynPrint(pasynUser, ASYN_TRACE_ERROR,
			"%s:%s: error, status=%d function=%d, addr=%d, value=%d\n",
			driverName, functionName, status, function, addr, *value);
	} else {
		asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
			"%s:%s: function=%d, addr=%d, value=%d\n",
			driverName, functionName, function, addr, *value);
	}

	return status;
}


asynStatus PRL_Amp_PDC::writeFloat64(asynUser *pasynUser, epicsFloat64 value) {

	int function = pasynUser->reason;
	int addr = 0;
	asynStatus status = asynSuccess;
	const char *functionName = "writeFloat64";

	status = getAddress(pasynUser, &addr);
	if (status != asynSuccess) {
		return(status);
	}

	D(printf("function %d, addr %d, value %f\n", function, addr, value));
	status = setDoubleParam(addr, function, value);

	if (function == PRLAmpPDC_TMPSP) {
		status = writeTemp(addr, value);
	} else if (function == PRLAmpPDC_PWRPID352) {
		status = writePowerPID(addr, value, PRLAMPPDC_PWRPID352_REG);
	} else if (function == PRLAmpPDC_PWRPID704) {
		status = writePowerPID(addr, value, PRLAMPPDC_PWRPID704_REG);
	}	
    
    /* Do callbacks so higher layers see any changes */
	callParamCallbacks(addr, addr);

	if (status) {
		asynPrint(pasynUser, ASYN_TRACE_ERROR,
			"%s:%s: error, status=%d function=%d, addr=%d, value=%f\n",
			driverName, functionName, status, function, addr, value);
	} else {
		asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
			"%s:%s: function=%d, addr=%d, value=%f\n",
			driverName, functionName, function, addr, value);
	}

	return status;
}


asynStatus PRL_Amp_PDC::writeInt32(asynUser *pasynUser, epicsInt32 value) {

	int function = pasynUser->reason;
	int addr = 0;
	asynStatus status = asynSuccess;
	const char *functionName = "writeInt32";

	status = getAddress(pasynUser, &addr);
	if (status != asynSuccess) {
		return(status);
	}

	status = setIntegerParam(addr, function, value);

// PDC funcitons

	if (function == PRLAmpPDC_RECAL) {
		status = writeRecalib(addr, value);
	} else if (function == PRLAmpPDC_COMP_CAB) {
		status = writeComp(addr, PRLAMPPDC_COMP_CAB_VAL);
	} else if (function == PRLAmpPDC_COMP_AMP) {
		status = writeComp(addr, PRLAMPPDC_COMP_AMP_VAL);
	} 
// Amplifier functions
//	if (function == PRL_Amp_PDC_Test) {
//		status = setTest(addr);
//    } else 
    else if (function == PRLAmpPDC_352_OnOff) {
        status = setOnOff(addr, value, 0);
    } else if (function == PRLAmpPDC_704_OnOff) {
        status = setOnOff(addr, value, 1);
    }
//    else if (function < FIRST_PRL_Amp_PDC_PARAM) {
//		/* If this parameter belongs to a base class call its method */
//		status = AKSPI::writeInt32(pasynUser, value);
//	}

	/* Do callbacks so higher layers see any changes */
	callParamCallbacks(addr, addr);

	if (status) {
		asynPrint(pasynUser, ASYN_TRACE_ERROR,
			"%s:%s: error, status=%d function=%d, addr=%d, value=%d\n",
			driverName, functionName, status, function, addr, value);
	} else {
		asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
			"%s:%s: function=%d, addr=%d, value=%d\n",
			driverName, functionName, function, addr, value);
	}

	return status;
}


//devAddrI2C : I2C Address 
PRL_Amp_PDC::PRL_Amp_PDC(const char *portName, const char *ipPortSPI,
        const char *ipPortI2C, int devAddrI2C, int priority, 
        int stackSize)
	: asynPortDriver(portName, 
        1, /*maxAddr*/
		asynInt32Mask | asynFloat64Mask | asynOctetMask | asynDrvUserMask,
		asynInt32Mask | asynFloat64Mask | asynOctetMask,
        ASYN_CANBLOCK, /*asynFlags*/
        1, /*autoConnect */
        priority, 
        stackSize)
{
    int status;

	/* Create an EPICS exit handler */
	epicsAtExit(exitHandler, this);
    mIpPortSPI = strdup(ipPortSPI);
    mIpPortI2C = strdup(ipPortI2C);
    addrI2C = devAddrI2C;

	/* Connect to desired IP port - SPI */
	status = pasynOctetSyncIO->connect(mIpPortSPI, 0, &mAsynUserSPI, NULL);
	if (status) {
		printf("pasynOctetSyncIO->connect failure\n");
		printf("SPI init FAIL!\n");
		disconnect(pasynUserSelf);
		return;
	}

	/* Connect to desired IP port - I2C */
	status = pasynOctetSyncIO->connect(mIpPortI2C, 0, &mAsynUserI2C, NULL);
	if (status) {
		printf("pasynOctetSyncIO->connect failure\n");
		printf("I2C init FAIL!\n");
		disconnect(pasynUserSelf);
		return;
	}

    //Create params
    
	createParam(StatusMessageString,	asynParamOctet,	&StatusMessage);
	setStringParam(StatusMessage, "");
    //PDs
	createParam(PRLAMPPDC_PD0String,		asynParamFloat64,		&PRLAmpPDC_PD0);
	createParam(PRLAMPPDC_PD1String,		asynParamFloat64,		&PRLAmpPDC_PD1);
	createParam(PRLAMPPDC_PD2String,		asynParamFloat64,		&PRLAmpPDC_PD2);
	createParam(PRLAMPPDC_PD3String,		asynParamFloat64,		&PRLAmpPDC_PD3);
	createParam(PRLAMPPDC_PD4String,		asynParamFloat64,		&PRLAmpPDC_PD4);
    
    //PDs PWR
	createParam(PRLAMPPDC_PD0PWRString,		asynParamFloat64,		&PRLAmpPDC_PD0PWR);
	createParam(PRLAMPPDC_PD1PWRString,		asynParamFloat64,		&PRLAmpPDC_PD1PWR);
	createParam(PRLAMPPDC_PD2PWRString,		asynParamFloat64,		&PRLAmpPDC_PD2PWR);
	createParam(PRLAMPPDC_PD3PWRString,		asynParamFloat64,		&PRLAmpPDC_PD3PWR);
	createParam(PRLAMPPDC_PD4PWRString,		asynParamFloat64,		&PRLAmpPDC_PD4PWR);

    //PWD
	createParam(PRLAMPPDC_PWD0String,		asynParamFloat64,		&PRLAmpPDC_PWD0);
	createParam(PRLAMPPDC_PWD1String,		asynParamFloat64,		&PRLAmpPDC_PWD1);
	createParam(PRLAMPPDC_PWD2String,		asynParamFloat64,		&PRLAmpPDC_PWD2);
	createParam(PRLAMPPDC_PWD3String,		asynParamFloat64,		&PRLAmpPDC_PWD3);

	createParam(PRLAMPPDC_TMPSPString,		asynParamFloat64,		&PRLAmpPDC_TMPSP);
	createParam(PRLAMPPDC_TMPString,		asynParamFloat64,		&PRLAmpPDC_TMP);
	createParam(PRLAMPPDC_HUMString,		asynParamInt32,			&PRLAmpPDC_HUM);

    createParam(PRLAMPPDC_PWRPID352String,		asynParamFloat64,		&PRLAmpPDC_PWRPID352);
    createParam(PRLAMPPDC_PWRPID704String,		asynParamFloat64,		&PRLAmpPDC_PWRPID704);

    createParam(PRLAMPPDC_RECALString,		asynParamInt32,		&PRLAmpPDC_RECAL);
    createParam(PRLAMPPDC_COMPString,		asynParamInt32,		&PRLAmpPDC_COMP);
    createParam(PRLAMPPDC_COMP_CABString,		asynParamInt32,		&PRLAmpPDC_COMP_CAB);
    createParam(PRLAMPPDC_COMP_AMPString,		asynParamInt32,		&PRLAmpPDC_COMP_AMP);
	createParam(PRLAMPPDC_PHERR352String,		asynParamFloat64,		&PRLAmpPDC_PHERR352);
	createParam(PRLAMPPDC_PHERR704String,		asynParamFloat64,		&PRLAmpPDC_PHERR704);
	createParam(PRLAMPPDC_ATTVOLT352String,		asynParamFloat64,		&PRLAmpPDC_ATTVOLT352);
	createParam(PRLAMPPDC_ATTVOLT704String,		asynParamFloat64,		&PRLAmpPDC_ATTVOLT704);
	createParam(PRLAMPPDC_PHSHIFTV352String,		asynParamFloat64,		&PRLAmpPDC_PHSHIFTV352);
	createParam(PRLAMPPDC_PHSHIFTV704String,		asynParamFloat64,		&PRLAmpPDC_PHSHIFTV704);

    //AMP Params
	createParam(PRLAMPPDC_352_InPowerString,		asynParamFloat64,	&PRLAmpPDC_352_InPwr);
	createParam(PRLAMPPDC_704_InPowerString,		asynParamFloat64,	&PRLAmpPDC_704_InPwr);
	createParam(PRLAMPPDC_352_RefPowerString,		asynParamFloat64,	&PRLAmpPDC_352_RefPwr);
	createParam(PRLAMPPDC_704_RefPowerString,		asynParamFloat64,	&PRLAmpPDC_704_RefPwr);
	createParam(PRLAMPPDC_352_OutPowerString,		asynParamFloat64,	&PRLAmpPDC_352_OutPwr);
	createParam(PRLAMPPDC_704_OutPowerString,		asynParamFloat64,	&PRLAmpPDC_704_OutPwr);
	createParam(PRLAMPPDC_352_TempString,		asynParamFloat64,	&PRLAmpPDC_352_Temp);
	createParam(PRLAMPPDC_704_TempString,		asynParamFloat64,	&PRLAmpPDC_704_Temp);

	createParam(PRLAMPPDC_352_TempStatString,		asynParamInt32,	&PRLAmpPDC_352_TempStat);
	createParam(PRLAMPPDC_704_TempStatString,		asynParamInt32,	&PRLAmpPDC_704_TempStat);
	createParam(PRLAMPPDC_352_AmpStatString,		asynParamInt32,	&PRLAmpPDC_352_AmpStat);
	createParam(PRLAMPPDC_704_AmpStatString,		asynParamInt32,	&PRLAmpPDC_704_AmpStat);

	createParam(PRLAMPPDC_352_OnOffString,		asynParamInt32,	&PRLAmpPDC_352_OnOff);
	createParam(PRLAMPPDC_704_OnOffString,		asynParamInt32,	&PRLAmpPDC_704_OnOff);
}

PRL_Amp_PDC::~PRL_Amp_PDC() {
	printf("shut down ..\n");

	pasynOctetSyncIO->disconnect(mAsynUserI2C);
	pasynOctetSyncIO->disconnect(mAsynUserSPI);
}

/* Configuration routine.  Called directly, or from the iocsh function below */

extern "C" {

int PRLAmpPDCConfigure(const char *portName, const char *ipPortSPI,
        const char *ipPortI2C, int devAddrI2C, int priority, 
        int stackSize){
	new PRL_Amp_PDC(portName, ipPortSPI, ipPortI2C, devAddrI2C, priority, stackSize);
	return(asynSuccess);
}

/* EPICS iocsh shell commands */

static const iocshArg initArg0 = { "portName",	    iocshArgString};
static const iocshArg initArg1 = { "ipPortSPI",		iocshArgString};
static const iocshArg initArg2 = { "ipPortI2C",		iocshArgString};
static const iocshArg initArg3 = { "devAddrI2C",	iocshArgInt};
static const iocshArg initArg4 = { "priority",		iocshArgInt};
static const iocshArg initArg5 = { "stackSize",		iocshArgInt};
static const iocshArg * const initArgs[] = {&initArg0,
											&initArg1,
											&initArg2,
											&initArg3,
											&initArg4,
											&initArg5};
static const iocshFuncDef initFuncDef = {"PRLAmpPDCConfigure", 6, initArgs};
static void initCallFunc(const iocshArgBuf *args) {
	PRLAmpPDCConfigure(args[0].sval, args[1].sval,
			args[2].sval, args[3].ival, args[4].ival, args[5].ival);
}

void PRLAmpPDCRegister(void) {
	iocshRegister(&initFuncDef, initCallFunc);
}

epicsExportRegistrar(PRLAmpPDCRegister);

} /* extern "C" */
